use std::{sync::Arc, time::Duration};

use crate::{table_builder::TableBuilder, vod_storage::VodStorage};

pub struct SharedContext {
    pub table_builder: TableBuilder,
    pub vod_storage: VodStorage,
}

impl SharedContext {
    pub fn new(html_template: String, vod_lifetime: Duration) -> Arc<Self> {
        Arc::new(Self {
            table_builder: TableBuilder::new(html_template),
            vod_storage: VodStorage::new(vod_lifetime),
        })
    }
}
