pub type Table = Vec<Vec<String>>;

pub struct TableBuilder {
    html_template: String,
    has_table: bool,
}

impl TableBuilder {
    pub fn new(html_template: String) -> Self {
        let has_table = html_template.contains("{table}");
        TableBuilder {
            html_template,
            has_table,
        }
    }

    pub fn build_html(&self, table: Table) -> String {
        if !self.has_table {
            return self.html_template.clone();
        }

        self.html_template
            .replace("{table}", &self.table_to_html(table))
    }

    fn table_to_html(&self, table: Table) -> String {
        let cap: usize = table
            .iter()
            .map(|row| row.iter().map(|field| field.len() + 10).sum::<usize>() + 10)
            .sum();
        let mut result = String::with_capacity(cap);
        for row in table {
            result += "<tr>\n";
            for field in row {
                result += "<td>";
                result += &field;
                result += "</td>\n";
            }
            result += "</tr>\n";
        }
        result
    }
}
