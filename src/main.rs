use std::{collections::HashMap, time::Duration};

use actix_web::{middleware, web, App, HttpServer};
use actix_web_static_files::ResourceFiles;
use clap::Parser;
use static_files::Resource;
use tracing::info;
use tracing_subscriber::{fmt, prelude::*, EnvFilter};

use crate::shared_context::SharedContext;
mod command_line_args;
mod get_vod_handler;
mod shared_context;
mod table_builder;
mod twitch_kraken;
mod vod_storage;

fn generate_statics() -> HashMap<&'static str, Resource> {
    include!(concat!(env!("OUT_DIR"), "/generated_statics.rs"))
}

fn generate_templates() -> HashMap<&'static str, Resource> {
    include!(concat!(env!("OUT_DIR"), "/generated_templates.rs"))
}

#[tokio::main]
async fn main() -> anyhow::Result<()> {
    tracing_subscriber::registry()
        .with(fmt::layer())
        .with(EnvFilter::from_default_env())
        .init();

    let cl_args = command_line_args::CommandLineArgs::parse();

    let resources = generate_templates();
    let shared_context = SharedContext::new(
        std::str::from_utf8(resources["twitch_vod_info.template_html"].data)
            .unwrap()
            .to_owned(),
        Duration::from_secs(cl_args.vod_cache_lifetime_sec),
    );

    info!("Starting server on {}", cl_args.bind_to);

    let res = HttpServer::new(move || {
        let resources = generate_statics();
        let mut logger_mw =
            middleware::Logger::new("Peer: [%a], Real: [%{r}a]: %r -> %s, took %D ms");
        if !cl_args.enable_middleware_logger {
            logger_mw = logger_mw.exclude_regex(".*");
        }
        App::new()
            .wrap(logger_mw)
            .app_data(web::Data::new(shared_context.clone()))
            .service(crate::get_vod_handler::get_vod)
            .service(ResourceFiles::new("/", resources))
    })
    .bind(cl_args.bind_to)?
    .run()
    .await;
    Ok(res?)
}
