use std::{cmp::Ordering, collections::HashMap};

use anyhow::{anyhow, Context};
use serde::{Deserialize, Serialize};
use url::Url;

use crate::table_builder;

#[derive(Debug, Clone, Serialize, Deserialize)]
#[serde(untagged)]
pub enum TwitchKrakenResponse {
    Success {
        animated_preview_url: Url,
        resolutions: HashMap<String, String>,
    },
    Fail {
        message: String,
    },
}

fn validate_resolutions<'a>(resolutions: impl Iterator<Item = &'a String>) -> anyhow::Result<()> {
    for resolution in resolutions {
        let w: i32;
        let h: i32;
        text_io::try_scan!(resolution.bytes() => "{}x{}", w, h);
    }
    Ok(())
}

fn compare_by_resolution(first: &str, second: &str) -> Ordering {
    let w1: i32;
    let h1: i32;
    let w2: i32;
    let h2: i32;

    text_io::scan!(first.bytes() => "{}x{}", w1, h1);
    text_io::scan!(second.bytes() => "{}x{}", w2, h2);

    (w1, h1).cmp(&(w2, h2))
}

impl TryInto<table_builder::Table> for TwitchKrakenResponse {
    type Error = anyhow::Error;

    fn try_into(self) -> Result<table_builder::Table, Self::Error> {
        match self {
            Self::Success {
                animated_preview_url,
                resolutions,
            } => {
                // quality - resolution
                validate_resolutions(resolutions.iter().map(|(_, r)| r))
                    .context("Invalid kraken response: invalid resolutions")?;
                let mut table = resolutions
                    .into_iter()
                    .map(|(quality, resolution)| {
                        let url = animated_preview_url
                            .join(format!("../{}/index-dvr.m3u8", quality).as_str())?
                            .to_string();
                        Ok(vec![
                            quality,
                            resolution,
                            format!(r#"<a href="{}" style="color:#e38e4d;">Watch</a>"#, url),
                        ])
                    })
                    .collect::<Result<table_builder::Table, Self::Error>>()?;

                table.sort_unstable_by(|row1, row2| compare_by_resolution(&row1[1], &row2[1]));
                Ok(table)
            }
            Self::Fail { message } => Err(anyhow!(message)),
        }
    }
}

const CLIENT_ID: &str = "kimne78kx3ncx6brgo4mv6wki5h1ko";
const ACCEPT: &str = "application/vnd.twitchtv.v5+json";
const RESOLVE_BASE_PATH: &str = "https://api.twitch.tv/kraken/videos/";

pub async fn fetch(vod_url: Url) -> anyhow::Result<TwitchKrakenResponse> {
    if !vod_url
        .domain()
        .context("Invalid twitch url")?
        .ends_with("twitch.tv")
    {
        return Err(anyhow!("Invalid domain: must be twitch.tv"));
    }

    let vod_id = vod_url
        .path_segments()
        .context("Invalid segments: must be like /videos/12345")?
        .last()
        .context("No vod id found in path segments")?
        .parse::<u64>()
        .context("Invalid vod id")?;

    let request_url = format!("{}{}", RESOLVE_BASE_PATH, vod_id);

    let client = reqwest::Client::new();
    let response_bytes = client
        .get(request_url)
        .header("Client-Id", CLIENT_ID)
        .header("Accept", ACCEPT)
        .send()
        .await?
        .bytes()
        .await?;

    let kraken_response: TwitchKrakenResponse = serde_json::from_slice(&response_bytes)
        .with_context(|| format!("Invalid kraken response: {:?}", response_bytes))?;
    Ok(kraken_response)
}
