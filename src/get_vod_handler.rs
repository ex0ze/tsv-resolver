use std::sync::Arc;

use actix_web::{
    error::{ErrorBadRequest, ErrorInternalServerError},
    get, web, HttpResponse, Responder,
};
use serde::Deserialize;
use tracing::{debug, warn};
use url::Url;

use crate::shared_context::SharedContext;

#[derive(Debug, Deserialize)]
pub struct GetVodQuery {
    vod_url: String,
}

#[get("/get_vod")]
pub async fn get_vod(
    shared_context: web::Data<Arc<SharedContext>>,
    query: web::Query<GetVodQuery>,
) -> impl Responder {
    debug!("Processing /get_vod for {}", query.vod_url);

    let Ok(url) = Url::parse(&query.vod_url) else {
        warn!("Invalid url for /get_vod: {}", query.vod_url);
        return Err(ErrorBadRequest("Invalid URL"));
    };
    let kraken_response =
        shared_context
            .vod_storage
            .fetch(url)
            .await
            .map_err(|e: anyhow::Error| {
                let error_string = e.to_string();
                warn!(
                    "Error while fetching {} from kraken: {}",
                    query.vod_url, error_string
                );
                ErrorInternalServerError(error_string)
            })?;

    let html_response =
        shared_context
            .table_builder
            .build_html(kraken_response.try_into().map_err(|e: anyhow::Error| {
                let error_string = e.to_string();
                warn!(
                    "Error while processing request for url {}: {}",
                    query.vod_url, error_string
                );
                ErrorInternalServerError(error_string)
            })?);

    debug!("Successfully handled {}", query.vod_url);
    Ok(HttpResponse::Ok().body(html_response))
}
