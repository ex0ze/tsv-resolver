use std::{
    sync::Arc,
    time::{Duration, Instant},
};

use dashmap::DashMap;
use reqwest::Url;
use tokio::sync::Mutex;
use tracing::debug;

use crate::twitch_kraken::TwitchKrakenResponse;

struct TimedVodEntry {
    response: TwitchKrakenResponse,
    created_at: Instant,
}

pub struct VodStorage {
    vods: DashMap<Url, Arc<Mutex<Option<TimedVodEntry>>>>,
    vod_lifetime: Duration,
}

impl VodStorage {
    pub fn new(vod_lifetime: Duration) -> Self {
        debug!("Vod Storage initialized with lifetime={:?}", vod_lifetime);

        Self {
            vods: Default::default(),
            vod_lifetime,
        }
    }

    fn is_expired(&self, entry: &TimedVodEntry) -> bool {
        entry.created_at.elapsed() >= self.vod_lifetime
    }

    pub async fn fetch(&self, url: Url) -> anyhow::Result<TwitchKrakenResponse> {
        let cached = self
            .vods
            .entry(url.clone())
            .or_insert_with(Default::default)
            .clone();

        let mut locked = cached.lock().await;
        match &*locked {
            Some(entry) if !self.is_expired(entry) => {
                debug!("Fetched response for url {} from cache", url);
                Ok(entry.response.clone())
            }
            _ => {
                if locked.is_some() {
                    debug!("Response for url {} is expired, refetch...", url);
                } else {
                    debug!("No response found for url {} in cache, fetching...", url);
                }
                let fetched = crate::twitch_kraken::fetch(url.clone()).await;
                match fetched {
                    Ok(resp) => {
                        debug!(
                            "Successfully fetched response for url {}, save in cache",
                            url
                        );
                        *locked = Some(TimedVodEntry {
                            response: resp.clone(),
                            created_at: Instant::now(),
                        });
                        Ok(resp)
                    }
                    Err(e) => {
                        debug!("Fetch for url {} failed, maybe next time...", url);
                        Err(e)
                    }
                }
            }
        }
    }
}
