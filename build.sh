#!/bin/bash

set -e

echo "Building TSV Resolver..."
cargo build --release

echo "Done!"
