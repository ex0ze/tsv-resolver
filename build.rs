use std::{env, path::Path};

use static_files::resource::generate_resources_mapping;

fn main() -> std::io::Result<()> {
    let out_dir = env::var("OUT_DIR").unwrap();
    let generated_statics = Path::new(&out_dir).join("generated_statics.rs");
    let generated_templates = Path::new(&out_dir).join("generated_templates.rs");
    generate_resources_mapping("./assets/static", None, generated_statics)?;
    generate_resources_mapping("./assets/templates", None, generated_templates)
}
