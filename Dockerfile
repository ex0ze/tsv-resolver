FROM rust:1.66-slim-buster

RUN apt update -y && apt install pkg-config libssl-dev -y

WORKDIR /app

ADD . .

RUN chmod +x build.sh
RUN ./build.sh

ENTRYPOINT [ "./target/release/tsv-resolver" ]
